using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using NiceSDK;
using DG.Tweening;
public class MovingUnit : Unit, IPointerClickHandler
{
    [SerializeField, ReadOnly] private Image _selectImage;
    [SerializeField, ReadOnly] private PathFinding _pathFinding;

    private Tween _selectTween;
    private Sequence _moveSequence;

#if UNITY_EDITOR
    [Button]
    protected override void setRef()
    {
        base.setRef();
        _selectImage = transform.FindDeepChild<Image>("SelectImage");
        _pathFinding = GetComponent<PathFinding>();

    }
#endif
    protected override void OnEnable()
    {
        base.OnEnable();
        _selectImage.color = new Color(_selectImage.color.r, _selectImage.color.g, _selectImage.color.b, 0);
        _isMoving = false;
    }
    public void OnPointerClick(PointerEventData eventData)
    {
        _levelManager.SelectSoldier(this);
    }

    public void Select()
    {
        _selectTween?.Kill();
        _selectImage.color = new Color(_selectImage.color.r, _selectImage.color.g, _selectImage.color.b, 0);
        _selectTween = _selectImage.DOFade(1,.3f).SetEase(Ease.InOutFlash).SetLoops(-1, LoopType.Yoyo);
    }

    public void Deselect()
    {
        _selectTween?.Kill();
        _selectTween = _selectImage.DOFade(0, .3f).SetEase(Ease.OutFlash);
    }
    public void Move(Cell cell)
    {
        if (_unitColliderArr[0].TargetCell != null)
        {
            if (_isMoving && !_unitColliderArr[0].TargetCell.IsAvailable)
            {
                return;
            }
            _isMoving = true;
            _unitColliderArr[0].TargetCell.Deoccupy();
            _pathFinding.FindPath(_unitColliderArr[0].TargetCell, cell);
            _moveSequence?.Kill();
            _moveSequence = DOTween.Sequence();
            for (int i = 0; i < _levelManager.Path.Count; i++)
            {
                _moveSequence.Append(transform.DOLocalMove(_levelManager.Path[i].transform.localPosition, .3f).SetEase(Ease.Linear));
            }
            _moveSequence.OnComplete(()=>
            {
                cell.Occupie();
                _isMoving = false;
            });
            _moveSequence.Play();
        }
    }
}