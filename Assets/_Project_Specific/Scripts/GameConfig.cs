using UnityEngine;
using System;
using Sirenix.OdinInspector;

namespace NiceSDK
{
    [CreateAssetMenu(fileName = "GameConfig")]
    public class GameConfig : SingletonScriptableObject<GameConfig>
    {
        public PlayerVariablesEditor Player = new PlayerVariablesEditor();
        public MapVariablesEditor Map = new MapVariablesEditor();
        public UnitVariablesEditor Unit = new UnitVariablesEditor();
        public CellVariablesEditor Cell = new CellVariablesEditor();
    }
    [Serializable]
    public class PlayerVariablesEditor
    {
    }
    [Serializable]
    public class MapVariablesEditor
    {
        [ReadOnly] public Vector2 CellSize;
    }
    [Serializable]
    public class UnitVariablesEditor
    {
        public DefaultTweenValue BuildScaleTween;
        public DefaultTween BuildMoveTween;
        public DefaultTween UnitActivationScaleTweenData;
        public float MovementDamping = .5f;
    }
    [Serializable]
    public class CellVariablesEditor
    {
        public Color32 ActiveColor;
        public Color32 OccupiedColor;
        public Color32 PathColor;
        public Color32 CheckColor;
        public Color32 DeniedColor;
        public Color32 PointerEnter;
        public Color32 SpawnColor;
    }
}