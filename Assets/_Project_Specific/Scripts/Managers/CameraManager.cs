using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NiceSDK;
using Sirenix.OdinInspector;

public class CameraManager : MonoBehaviourSingleton<CameraManager>
{
    [SerializeField, ReadOnly] private Camera _mainCamera;

    public Camera MainCamera => _mainCamera;
#if UNITY_EDITOR
    private void OnValidate()
    {
        SetRef();
    }

    [Button]
    private void SetRef()
    {
        _mainCamera = GetComponent<Camera>();
    }
#endif
}