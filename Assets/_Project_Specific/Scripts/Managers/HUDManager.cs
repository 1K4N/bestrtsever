using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Sirenix.OdinInspector;
using UnityEngine.EventSystems;

namespace NiceSDK
{
    public class HUDManager : MonoBehaviourSingleton<HUDManager>
    {
        [SerializeField, ReadOnly] private Canvas _canvas;
        [SerializeField, ReadOnly] private UnitMenu[] _unitMenus;

        [SerializeField, ReadOnly] private Transform _scrollBorderUp;
        [SerializeField, ReadOnly] private Transform _scrollBorderDown;
        [SerializeField] private Transform scrollPanel;
        private UnitMenu _openedMenu;
        private UnitButton _createdUnitButton;
        private int _detectedScrollObjectCount;

        [SerializeField] private ScrollUpdater[] _defaultScrollUpdaters;
        [SerializeField] private List<ScrollUpdater> _scrollUpdaterList = new List<ScrollUpdater>();
        public Transform ScrollBorderUp => _scrollBorderUp;
        public Transform ScrollBorderDown => _scrollBorderDown;
        public Canvas Canvas => _canvas;
#if UNITY_EDITOR
        private void OnValidate()
        {
            SetRef();
        }

        [Button]
        private void SetRef()
        {
            _canvas = GetComponent<Canvas>();
            _unitMenus = GetComponentsInChildren<UnitMenu>(true);
            _scrollBorderUp = transform.FindDeepChild<Transform>("ScrollBorderUp");
            _scrollBorderDown = transform.FindDeepChild<Transform>("ScrollBorderDown");
            _defaultScrollUpdaters = GetComponentsInChildren<ScrollUpdater>(true);
            _scrollUpdaterList.Clear();
            for (int i = 0; i < _defaultScrollUpdaters.Length; i++)
            {
                _scrollUpdaterList.Add(_defaultScrollUpdaters[i]);
            }
        }
#endif
        private void OnEnable()
        {
            closeAllMenus();
            _detectedScrollObjectCount = 0;
        }

        public override void OnDisable()
        {
            base.OnDisable();
        }

        #region CLASS FUNCTIONS
        public void ActivateUnitButtonUpdaters()
        {
            foreach (ScrollUpdater scrollUpdater in _scrollUpdaterList)
            {
                if (scrollPanel.GetChild(0) == scrollUpdater.transform || scrollPanel.GetChild(scrollPanel.childCount - 1) == scrollUpdater.transform)
                {
                    scrollUpdater.enabled = true;
                }
            }
        }
        public void DeactivateUnitButtonUpdaters()
        {
            for (int i = 0; i < _scrollUpdaterList.Count; i++)
            {
                _scrollUpdaterList[i].enabled = false;
            }
        }
        public void CreateNewButtons()
        {
            switch (_detectedScrollObjectCount)
            {
                case 0:
                    _createdUnitButton = PoolManager.CreateShipyardButton(scrollPanel, scrollPanel.position);
                    _createdUnitButton.transform.SetAsFirstSibling();
                    updateCreatedUnitButton();
                    _createdUnitButton = PoolManager.CreateBarrackButton(scrollPanel, scrollPanel.position);
                    updateCreatedUnitButton();
                    break;
                case 1:
                    _createdUnitButton = PoolManager.CreatePowerPlantButton(scrollPanel, scrollPanel.position);
                    _createdUnitButton.transform.SetAsFirstSibling();
                    updateCreatedUnitButton();
                    _createdUnitButton = PoolManager.CreatePowerPlantButton(scrollPanel, scrollPanel.position);
                    updateCreatedUnitButton();
                    break;
                case 2:
                    _createdUnitButton = PoolManager.CreateBarrackButton(scrollPanel, scrollPanel.position);
                    _createdUnitButton.transform.SetAsFirstSibling();
                    updateCreatedUnitButton();
                    _createdUnitButton = PoolManager.CreateShipyardButton(scrollPanel, scrollPanel.position);
                    updateCreatedUnitButton();
                    break;
                default:
                    break;
            }
            _detectedScrollObjectCount++;
            if (_detectedScrollObjectCount == 3)
            {
                _detectedScrollObjectCount = 0;
            }
        }
        private void updateCreatedUnitButton()
        {
            _createdUnitButton.transform.localScale = Vector3.one;
            _scrollUpdaterList.Add(_createdUnitButton.ScrollUpdater);
        }
        public void DeactivateOpenedMenu() => closeMenu();
        public void ActivateUnitMenu(Unit unit) => openMenu(_unitMenus[0], unit);
        private void openMenu(UnitMenu menu, Unit building)
        {
            _openedMenu = menu;            
            _openedMenu.Activate();
            _openedMenu.SetUnit(building);
        }
        private void closeMenu()
        {
            if (_openedMenu != null)
            {
                _openedMenu.gameObject.SetActive(false);
                _openedMenu = null;
            }
        }
        private void closeAllMenus()
        {
            for (int i = 0; i < _unitMenus.Length; i++)
            {
                _unitMenus[i].gameObject.SetActive(false);
            }
        }
        #endregion
    }
}