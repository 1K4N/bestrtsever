using System.Collections.Generic;
using UnityEngine;
using System;
using Sirenix.OdinInspector;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace NiceSDK
{
    [Serializable]
    public class DictionaryPoolTypeListPoolConfig : UnitySerializedDictionary<ePoolType, PoolConfig> { }

    public class PoolManagerBase : MonoBehaviourSingleton<PoolManagerBase>
    {

        [SerializeField] public DictionaryPoolTypeListPoolConfig PoolItemsDefinition = new DictionaryPoolTypeListPoolConfig();

        [ReadOnly, Header("Pool Objects")] public DictionaryPoolTypeListGameObject ActiveObjects = new DictionaryPoolTypeListGameObject();
        [ReadOnly] public DictionaryPoolTypeListGameObject AvailableObjects = new DictionaryPoolTypeListGameObject();

        [ReadOnly, Header("Pool Holders")] public Transform PoolHolder;
        [ReadOnly] public DictionaryPoolTypeListTransform PoolHolderType = new DictionaryPoolTypeListTransform();

        // Dummies
        private Transform _dummyTransform;
        private GameObject _dummyGameObject;

        protected override void OnAwakeEvent()
        {
            base.OnAwakeEvent();
            createFromExistingTransforms();
        }

        [Button]
        private void prebake()
        {
            removeTransforms();
            createTransforms();
        }

        private void removeTransforms()
        {
            for (int i = transform.childCount - 1; i >= 0; i--)
            {
                DestroyImmediate(transform.GetChild(i).gameObject);
            }

            clearLists();
        }
        private void createTransforms()
        {
            PoolHolder = new GameObject("Pool Holder").transform;
            PoolHolder.transform.parent = transform;

            foreach (KeyValuePair<ePoolType, PoolConfig> item in PoolItemsDefinition)
            {
                _dummyTransform = new GameObject(item.Key.ToString()).transform;
                _dummyTransform.SetParent(PoolHolder);
                PoolHolderType.Add(item.Key, _dummyTransform);

                List<GameObject> _dummyList = new List<GameObject>();

                for (int i = 0; i < item.Value.InitialQuantity; i++)
                {
                    if (item.Value.Prefab == null)
                    {
                        Debug.LogError("Prefab is Null. " + item.Key + " prefab.");
                        continue;
                    }
                    else
                    {
#if UNITY_EDITOR
                        _dummyGameObject = PrefabUtility.InstantiatePrefab(item.Value.Prefab) as GameObject;
#endif
                        _dummyGameObject.SetActive(false);
                        _dummyGameObject.name = item.Key + " Pool " + i;
                        _dummyGameObject.transform.SetParent(PoolHolderType[item.Key]);
                        _dummyList.Add(_dummyGameObject);
                    }
                }

                AvailableObjects.Add(item.Key, new GameObjectList(_dummyList));
                ActiveObjects.Add(item.Key, new GameObjectList());
            }
        }

        private void createFromExistingTransforms()
        {
            clearLists();
            rebuildLists();
        }

        private void rebuildLists()
        {
            foreach (KeyValuePair<ePoolType, PoolConfig> item in PoolItemsDefinition)
            {
                _dummyTransform = transform.FindDeepChild<Transform>(item.Key.ToString());
                PoolHolderType.Add(item.Key, _dummyTransform);

                List<GameObject> _dummyList = new List<GameObject>();

                for (int i = 0; i < _dummyTransform.childCount; i++)
                {
                    if (_dummyTransform.GetChild(i) != PoolHolder)
                        _dummyList.Add(_dummyTransform.GetChild(i).gameObject);
                }

                AvailableObjects.Add(item.Key, new GameObjectList(_dummyList));
                ActiveObjects.Add(item.Key, new GameObjectList());
            }
        }

        [Button]
        private void clearLists()
        {
            ActiveObjects.Clear();
            AvailableObjects.Clear();
            PoolHolderType.Clear();
        }

        public GameObject Dequeue(ePoolType type)
        {
            _dummyGameObject = dequeueObject(type);

            _dummyGameObject.SetActive(true);

            return _dummyGameObject;
        }
        public GameObject Dequeue(ePoolType type, Vector3 position)
        {
            _dummyGameObject = dequeueObject(type);
            _dummyGameObject.transform.position = position;
            _dummyGameObject.SetActive(true);

            return _dummyGameObject;
        }
        public GameObject Dequeue(ePoolType type, Transform parent, Vector3 position)
        {
            _dummyGameObject = dequeueObject(type);
            _dummyGameObject.transform.SetParent(parent);
            _dummyGameObject.transform.position = position;
            _dummyGameObject.SetActive(true);

            return _dummyGameObject;
        }

        private GameObject dequeueObject(ePoolType type)
        {
            _dummyGameObject = null;

            if (AvailableObjects[type].List.Count > 0)
            {
                _dummyGameObject = AvailableObjects[type].List[AvailableObjects[type].List.Count - 1];
                AvailableObjects[type].List.Remove(_dummyGameObject);
            }
            else
            {
                _dummyGameObject = Instantiate(PoolItemsDefinition[type].Prefab);


                if (_dummyGameObject == null)
                {
                    Debug.LogError("Prefab is Null. " + type);
                    return null;
                }

                _dummyGameObject.name = type + " Pool ";
                _dummyGameObject.transform.SetParent(PoolHolderType[type]);
            }

            ActiveObjects[type].List.Add(_dummyGameObject);

            return _dummyGameObject;
        }
        public void Queue(ePoolType type, GameObject i_object)
        {
            if (ActiveObjects[type].List.Contains(i_object))
                ActiveObjects[type].List.Remove(i_object);
            if (AvailableObjects[type].List.Contains(i_object))
                return;
            AvailableObjects[type].List.Add(i_object);

            i_object.transform.SetParent(PoolHolderType[type]);
            i_object.transform.position = Vector3.zero;
            i_object.transform.rotation = Quaternion.identity;

            i_object.name = type + " Pool ";
            i_object.SetActive(false);
        }
    }
}