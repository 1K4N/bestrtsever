using System.Collections.Generic;
using UnityEngine;
using NiceSDK;
using Sirenix.OdinInspector;
using UnityEngine.UI;

public class LevelManager : MonoBehaviourSingleton<LevelManager>
{
    [SerializeField, ReadOnly] private Map _map;
    [SerializeField, ReadOnly] private Transform _buildingParent;
    [SerializeField, ReadOnly] private HUDManager _hudManager;
    [SerializeField, ReadOnly] private Button _exitButton;

    private MovingUnit _selectedSoldier;
    private bool _isMoveKeyDown;

    private Cell _spawnCell;
    public bool IsMoveKeyDown => _isMoveKeyDown;
    public Cell SpawnCell => _spawnCell;
    public MovingUnit SelectedSoldier => _selectedSoldier;
    public Map Map => _map;
    public Transform BuildingParent => _buildingParent;
    public List<Cell> Path = new List<Cell>();
#if UNITY_EDITOR
    [Button]
    private void setRef()
    {
        _map = GetComponentInChildren<Map>(true);
        _buildingParent = transform.FindDeepChild<Transform>("BuildingTempParent");
        _hudManager = GetComponentInChildren<HUDManager>(true);
        _exitButton = transform.FindDeepChild<Button>("Exit");
    }
    private void OnValidate()
    {
        setRef();
    }
#endif
    #region UNITY INIT
    private void OnEnable()
    {
        Cursor.lockState = CursorLockMode.Confined;
        _exitButton.onClick.AddListener(Application.Quit);
    }
    #endregion
    #region UNITY LOOP
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Mouse1))
        {
            _isMoveKeyDown = true;
        }
        else if (Input.GetKeyUp(KeyCode.Mouse1))
        {
            _isMoveKeyDown = false;
        }
    }
    #endregion
    #region CLASS FUNCTIONS
    public void ShowPath()
    {
        if (Path.Count > 0)
        {
            for (int i = 0; i < Path.Count; i++)
            {
                Path[i].ShowPath();
            }
        }
    }
    public void ResetPath()
    {
        if (Path.Count > 0)
        {
            for (int i = 0; i < Path.Count; i++)
            {
                Path[i].ResetColor();
            }
        }
    }
    public void ActivateUnitMenu(Unit unit)
    {
        _hudManager.ActivateUnitMenu(unit);
    }
    public void SetSpawnCell(Cell cell)
    {
        DeactivateSpawnIndicator();
        _spawnCell = cell;
        _spawnCell.IndicateSpawn();
    }
    public void DeactivateSpawnIndicator()
    {
        if (_spawnCell != null)
        {
            _spawnCell.StopSpawnIndicator();
            _spawnCell = null;
        }
    }
    public void SelectSoldier(MovingUnit soldier)
    {
        DeselectSoldier();
        _selectedSoldier = soldier;
        soldier.Select();
    }
    public void DeselectSoldier()
    {
        if (_selectedSoldier != null)
        {
            _selectedSoldier.Deselect();
            _selectedSoldier = null;
        }
    }
    #endregion
}