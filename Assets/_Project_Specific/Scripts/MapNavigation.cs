using UnityEngine;
using Sirenix.OdinInspector;
using UnityEngine.EventSystems;

public class MapNavigation : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    [SerializeField] private eDirection _eDirection;
    [SerializeField, HideInInspector] private Vector3 _direction;
    private LevelManager _levelManager;
    private enum eDirection
    {
        North,
        South,
        West,
        East,
        NorthWest,
        NorthEast,
        SouthWest,
        SouthEast
    }

#if UNITY_EDITOR
    [Button]
    private void setRef()
    {
        switch (_eDirection)
        {
            case eDirection.North:
                _direction = Vector3.down;
                break;
            case eDirection.South:
                _direction = Vector3.up;
                break;
            case eDirection.West:
                _direction = Vector3.right;
                break;
            case eDirection.East:
                _direction = Vector3.left;
                break;
            case eDirection.NorthWest:
                _direction = new Vector3(1, -1, 0).normalized;
                break;
            case eDirection.NorthEast:
                _direction = new Vector3(-1, -1, 0).normalized;
                break;
            case eDirection.SouthWest:
                _direction = new Vector3(1, 1, 0).normalized;
                break;
            case eDirection.SouthEast:
                _direction = new Vector3(-1, 1, 0).normalized;
                break;
            default:
                break;
        }
    }
    private void OnValidate()
    {
        setRef();
    }
#endif

    #region UNITY INIT
    private void Awake()
    {
        _levelManager = LevelManager.Instance;
    }
    #endregion
    #region EVENTS
    public void OnPointerEnter(PointerEventData eventData)
    {
        _levelManager.Map.Navigate(_direction);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        _levelManager.Map.Navigate(Vector3.zero);
    }
    #endregion
}