using UnityEngine;

namespace NiceSDK
{
    public class PoolManager : PoolManagerBase
    {
        public static Barrack CreateBarrack(Transform parent, Vector3 position) => Instance.Dequeue(ePoolType.Barrack, parent, position).GetComponent<Barrack>();
        public static void ReturnBarrack(Barrack barrack) => Instance.Queue(ePoolType.Barrack, barrack.gameObject);

        public static PowerPlant CreatePowerPlant(Transform parent, Vector3 position) => Instance.Dequeue(ePoolType.PowerPlant, parent, position).GetComponent<PowerPlant>();
        public static void ReturnPowerPlant(PowerPlant powerPlant) => Instance.Queue(ePoolType.PowerPlant, powerPlant.gameObject);

        public static MovingUnit CreateSoldier(Transform parent, Vector3 position) => Instance.Dequeue(ePoolType.Soldier, parent, position).GetComponent<MovingUnit>();
        public static void ReturnSoldier(MovingUnit soldier) => Instance.Queue(ePoolType.Soldier, soldier.gameObject);

        public static Shipyard CreateShipyard(Transform parent, Vector3 position) => Instance.Dequeue(ePoolType.Shipyard, parent, position).GetComponent<Shipyard>();
        public static void ReturnShipyard(Shipyard shipyard) => Instance.Queue(ePoolType.Shipyard, shipyard.gameObject);

        public static MovingUnit CreateBoat(Transform parent, Vector3 position) => Instance.Dequeue(ePoolType.Boat, parent, position).GetComponent<MovingUnit>();
        public static void ReturnBoat(MovingUnit boat) => Instance.Queue(ePoolType.Boat, boat.gameObject);

        public static UnitButton CreateBarrackButton(Transform parent, Vector3 position) => Instance.Dequeue(ePoolType.BarrackButton, parent, position).GetComponent<UnitButton>();
        public static void ReturnBarrackButton(UnitButton barrackButton) => Instance.Queue(ePoolType.BarrackButton, barrackButton.gameObject);

        public static UnitButton CreatePowerPlantButton(Transform parent, Vector3 position) => Instance.Dequeue(ePoolType.PowerPlantButton, parent, position).GetComponent<UnitButton>();
        public static void ReturnPowerPlantButton(UnitButton powerPlantButton) => Instance.Queue(ePoolType.PowerPlantButton, powerPlantButton.gameObject);

        public static UnitButton CreateShipyardButton(Transform parent, Vector3 position) => Instance.Dequeue(ePoolType.ShipyardButton, parent, position).GetComponent<UnitButton>();
        public static void ReturnShipyardButton(UnitButton shipyardButton) => Instance.Queue(ePoolType.ShipyardButton, shipyardButton.gameObject);
    }
}