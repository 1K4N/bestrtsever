using UnityEngine.Events;
using UnityEngine.EventSystems;

public class PassiveUnit : Unit, IPointerClickHandler
{
    public void OnPointerClick(PointerEventData eventData)
    {
        _levelManager.ActivateUnitMenu(this);
    }
    public override void Build(UnityAction action)
    {
        base.Build(() =>
        {
            _levelManager.ActivateUnitMenu(this);
        });
    }
}
