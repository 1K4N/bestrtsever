using System;
using UnityEngine;

namespace NiceSDK
{
    [Serializable]
    public class PoolConfig
    {
        public GameObject Prefab;
        public int InitialQuantity;
    }
}
