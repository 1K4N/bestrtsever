using UnityEngine;
using Sirenix.OdinInspector;
using DG.Tweening;
using UnityEngine.UI;

public class UnitMenu : MonoBehaviour
{
    [SerializeField] private DefaultTween _activationTweenData;

    [SerializeField, ReadOnly] private UnitSubMenu[] _unitSubMenus;
    [SerializeField, ReadOnly] private RectTransform _rectTransform;
    [SerializeField, ReadOnly] private Vector2 _startPosition;

    private Unit _targetBuilding;
    private UnitSubMenu _currentSubMenu;
#if UNITY_EDITOR
    [Button]
    private void setRef()
    {
        _rectTransform = GetComponent<RectTransform>();
        _startPosition = _rectTransform.anchoredPosition;
        _unitSubMenus = GetComponentsInChildren<UnitSubMenu>(true);
    }
    private void OnValidate()
    {
        setRef();
    }
#endif
    #region CLASS FUNCTIONS
    public void Activate()
    {
        if (!gameObject.activeSelf)
        {
            _rectTransform.anchoredPosition = new Vector2(-_startPosition.x, _startPosition.y);
            gameObject.SetActive(true);
            _rectTransform.DOAnchorPosX(_startPosition.x, _activationTweenData.Duration).SetEase(_activationTweenData.Ease);
        }
    }
    public void SetUnit(Unit building)
    {
        deactivateSubMenus();
        _currentSubMenu = _unitSubMenus[(int)building.Type];
        _currentSubMenu.gameObject.SetActive(true);
        _targetBuilding = building;
        _currentSubMenu.SetButton(_targetBuilding.SpawnUnit);
    }
    private void deactivateSubMenus()
    {
        for (int i = 0; i < _unitSubMenus.Length; i++)
        {
            _unitSubMenus[i].gameObject.SetActive(false);
        }
    }
    #endregion
}
