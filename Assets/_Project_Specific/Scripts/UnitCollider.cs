using UnityEngine;
using NiceSDK;

public class UnitCollider : MonoBehaviour
{
    private Collider2D _enteredCollider;
    private Cell _targetCell;
    private Cell _collidedCell;
    private Unit _parentUnit;
    public Cell TargetCell => _targetCell;
    public bool Ready => (_targetCell != null) && (_targetCell.IsAvailable);

    #region PHYSICS
    private void OnTriggerExit2D(Collider2D collider2D)
    {
        switch (collider2D.tag)
        {
            case nameof(eTags.Cell):
                _collidedCell = collider2D.GetComponent<Cell>();
                if ((!_parentUnit.IsMoving && !_collidedCell.IsOnPath) || (_parentUnit.IsMoving && _collidedCell.IsOnPath))
                {
                    _collidedCell.ResetColor();
                }
                _targetCell = null;
                _enteredCollider = null;
                break;
            default:
                break;
        }
    }
    private void OnTriggerStay2D(Collider2D collider2D)
    {
        switch (collider2D.tag)
        {
            case nameof(eTags.Cell):
                if (_enteredCollider != collider2D)
                {
                    _enteredCollider = collider2D;
                    _targetCell = _enteredCollider.GetComponent<Cell>();
                    if ((!_parentUnit.IsMoving && !_targetCell.IsOnPath) || (_parentUnit.IsMoving && _targetCell.IsOnPath))
                    {
                        _targetCell.CheckAvailability();
                    }
                }
                break;
            default:
                break;
        }
    }
    #endregion
    #region CLASS FUNCTIONS
    public void SetUnitParent(Unit unit)
    {
        _parentUnit = unit;
    }
    #endregion
}
