using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class UnitSubMenu : MonoBehaviour
{
    [SerializeField, ReadOnly] private Button _unitCreationButton;

#if UNITY_EDITOR
    [Button]
    private void setRef()
    {
        _unitCreationButton = GetComponentInChildren<Button>(true);
    }
    private void OnValidate()
    {
        setRef();
    }
#endif
    public void SetButton(UnityAction action)
    {
        if (_unitCreationButton != null)
        {
            _unitCreationButton.onClick.RemoveAllListeners();
            _unitCreationButton.onClick.AddListener(action);
        }
    }
}
