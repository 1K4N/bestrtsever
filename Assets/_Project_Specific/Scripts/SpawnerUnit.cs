using NiceSDK;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;


public class SpawnerUnit : Unit, IPointerClickHandler
{
    protected MovingUnit _spawnedMovingUnit;
    public void OnPointerClick(PointerEventData eventData)
    {
        _levelManager.ActivateUnitMenu(this);
        indicateSpawnCell();
    }
    public override void SpawnUnit()
    {
        if (_levelManager.SpawnCell != null)
        {
            setMovingUnit();
            _spawnedMovingUnit.transform.position = _levelManager.SpawnCell.transform.position;
            _spawnedMovingUnit.ActivateBuilding();
            _levelManager.SpawnCell.Occupie();
            indicateSpawnCell();
        }
    }
    protected virtual void setMovingUnit()
    {

    }
    public override void Build(UnityAction action)
    {
        base.Build(() =>
        {
            _levelManager.ActivateUnitMenu(this);
            indicateSpawnCell();
        });
    }

}
