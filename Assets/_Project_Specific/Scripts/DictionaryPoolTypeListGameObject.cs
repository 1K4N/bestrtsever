using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class DictionaryPoolTypeListGameObject : UnitySerializedDictionary<ePoolType, GameObjectList>
{
}
[Serializable]
public class DictionaryPoolTypeListTransform : UnitySerializedDictionary<ePoolType, Transform>
{
}
[Serializable]
public class GameObjectList
{
    public List<GameObject> List;

    public GameObjectList()
    {
        List = new List<GameObject>();
    }
    public GameObjectList(List<GameObject> list)
    {
        List = list;
    }
}
