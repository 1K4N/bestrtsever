using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using NiceSDK;
using Sirenix.OdinInspector;
using DG.Tweening;
using UnityEngine.UI;
public class Cell : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler, IPointerDownHandler, IHeapItem<Cell>
{
    [SerializeField, ReadOnly] private RectTransform _rectTransform;
    [SerializeField, ReadOnly] private Image _image;
    [SerializeField, ReadOnly] private int _index;
    [SerializeField, ReadOnly] private int _indexX;
    [SerializeField, ReadOnly] private int _indexY;
    private HUDManager _hudManager;
    private LevelManager _levelManager;
    private GameConfig _gameConfig;
    private Canvas _canvas;
    private Tween _colorChange;
    private Tween _spawnTween;
    private Color32 _currentColor;
    private int _heapIndex;
    private int _compare;

    private bool _isAvailable;
    private bool _isOnPath;
    private bool _isSpawnIndicator;
    private bool _isEntered;

    public List<Cell> NeighborCells = new List<Cell>();
    public Cell ParentCell;
    public int GCost;
    public int HCost;
    public int FCost => GCost + HCost;
    public int Index => _index;
    public int IndexX => _indexX;
    public int IndexY => _indexY;
    public bool IsAvailable => _isAvailable;
    public bool IsOnPath => _isOnPath;
    public int HeapIndex { get => _heapIndex; set => _heapIndex = value; }

#if UNITY_EDITOR
    [Button]
    private void setRef()
    {
        _rectTransform = GetComponent<RectTransform>();
        _image = GetComponent<Image>();
        GameConfig.Instance.Map.CellSize = _rectTransform.sizeDelta;
    }
    private void OnValidate()
    {
        setRef();
    }
#endif

    #region UNITY INIT
    private void Awake()
    {
        _hudManager = HUDManager.Instance;
        _levelManager = LevelManager.Instance;
        _gameConfig = GameConfig.Instance;
        _canvas = _hudManager.Canvas;

        createNeighbors(_index + 1);
        createNeighbors(_index - 1);
        createNeighbors(_index - _levelManager.Map.SizeY);
        createNeighbors(_index - _levelManager.Map.SizeY + 1);
        createNeighbors(_index - _levelManager.Map.SizeY - 1);
        createNeighbors(_index + _levelManager.Map.SizeY);
        createNeighbors(_index + _levelManager.Map.SizeY + 1);
        createNeighbors(_index + _levelManager.Map.SizeY - 1);
    }
    private void OnEnable()
    {
        _isAvailable = true;
        _isOnPath = false;
        _isEntered = false;
        _currentColor = _gameConfig.Cell.ActiveColor;
    }
    #endregion
    #region EVENTS
    public void OnPointerEnter(PointerEventData eventData)
    {
        if (_isAvailable && !_isSpawnIndicator && !_isOnPath && !_levelManager.IsMoveKeyDown)
        {
            changeColor(_gameConfig.Cell.PointerEnter);
            _isEntered = true;
        }
        if (_levelManager.IsMoveKeyDown)
        {
            if (_levelManager.SelectedSoldier != null && _isAvailable)
            {
                _levelManager.SelectedSoldier.Move(this);
            }
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (_isAvailable && !_isOnPath && _isEntered)
        {
            changeColor(_currentColor);
            _isEntered = false;
        }
    }
    public void OnPointerClick(PointerEventData eventData)
    {
        _hudManager.DeactivateOpenedMenu();
    }
    public void OnPointerDown(PointerEventData eventData)
    {
        StartCoroutine(inputCheck());
    }
    private IEnumerator inputCheck()
    {
        yield return new WaitForEndOfFrame();
        if (_levelManager.IsMoveKeyDown)
        {
            if (_levelManager.SelectedSoldier != null && _isAvailable)
            {
                _levelManager.SelectedSoldier.Move(this);
            }
        }
    }
    #endregion
    #region CLASS FUNCTIONS
    public void SetIndex(int index)
    {
        _index = index;
        _indexX = _index / LevelManager.Instance.Map.SizeY;
        _indexY = _index % LevelManager.Instance.Map.SizeY;
    }
    public void IndicateSpawn()
    {
        _isSpawnIndicator = true;
        _spawnTween?.Kill();
        _colorChange?.Kill();
        _image.color = _gameConfig.Cell.ActiveColor;
        _spawnTween = _image.DOColor(_gameConfig.Cell.SpawnColor, .3f).SetEase(Ease.InOutFlash).SetLoops(-1, LoopType.Yoyo);
    }
    public void StopSpawnIndicator()
    {
        _isSpawnIndicator = false;
        _spawnTween?.Kill();
        changeColor(_currentColor);
    }
    public void CheckAvailability()
    {
        if (_isAvailable)
        {
            changeColor(_gameConfig.Cell.CheckColor);
        }
        else
        {
            changeColor(_gameConfig.Cell.DeniedColor);
        }
    }
    public void ResetColor()
    {
        _isOnPath = false;
        if (_isAvailable)
        {
            _currentColor = _gameConfig.Cell.ActiveColor;
        }
        else
        {
            _currentColor = _gameConfig.Cell.OccupiedColor;
        }
        changeColor(_currentColor);
    }
    public void Occupie()
    {
        _isAvailable = false;
        _currentColor = _gameConfig.Cell.OccupiedColor;
        changeColor(_currentColor);
    }
    public void Deoccupy()
    {
        _isAvailable = true;
        _currentColor = _gameConfig.Cell.ActiveColor;
        changeColor(_currentColor);
    }
    public void ShowPath()
    {
        changeColor(_gameConfig.Cell.PathColor);
        _isOnPath = true;
    }
    public void changeColor(Color32 color)
    {
        _spawnTween?.Kill();
        _colorChange?.Kill();
        _colorChange = _image.DOColor(color, .1f).SetEase(Ease.Flash);
    }
    private void createNeighbors(int index)
    {
        if (index < _levelManager.Map.CellCount && index >= 0)
        {
            if (Mathf.Abs(_levelManager.Map.GetCell(_index).IndexY - _levelManager.Map.GetCell(index).IndexY) > 1 ||
                Mathf.Abs(_levelManager.Map.GetCell(_index).IndexX - _levelManager.Map.GetCell(index).IndexX) > 1)
            {
                return;
            }
            NeighborCells.Add(_levelManager.Map.GetCell(index));
        }
    }

    public int CompareTo(Cell cell)
    {
        _compare = FCost.CompareTo(cell.FCost);
        if (_compare == 0)
        {
            _compare = HCost.CompareTo(cell.HCost);
        }
        return -_compare;
    }
    #endregion
}
