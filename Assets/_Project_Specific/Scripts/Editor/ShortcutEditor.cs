using UnityEngine;
using UnityEditor;

namespace NiceSDK
{
    public class ShortcutEditor : MonoBehaviour
    {
        [MenuItem("NiceSDK/Select GameConfig #%t", false, -2)]
        public static void SelectGameConfg()
        {
            Selection.activeObject = GameConfig.Instance;
        }
    }
}
