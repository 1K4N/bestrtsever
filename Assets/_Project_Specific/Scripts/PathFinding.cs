using System.Collections.Generic;
using UnityEngine;
public class PathFinding : MonoBehaviour
{
    private LevelManager _levelManager;

    private Cell _startCell;
    private Cell _targetCell;
    private Cell _currentCell;
    private Cell _currentRetracedCell;
    private Heap<Cell> _openSetList;
    private HashSet<Cell> _closedSet = new HashSet<Cell>();

    private int _cellDistanceX;
    private int _cellDistanceY;
    private void Awake()
    {
        _levelManager = LevelManager.Instance;
        _openSetList = new Heap<Cell>(_levelManager.Map.Size);
}
    public void FindPath(Cell startCell, Cell targetCell)
    {
        _startCell = startCell;
        _targetCell = targetCell;

        _openSetList.Clear();
        _closedSet.Clear();

        _openSetList.Add(_startCell);

        while (_openSetList.Count > 0)
        {
            _currentCell = _openSetList.RemoveFirst();

            _closedSet.Add(_currentCell);

            if (_currentCell == _targetCell)
            {
                retracePath(_startCell, _targetCell);

                return;
            }

            foreach (Cell neighbor in _currentCell.NeighborCells)
            {
                if (!neighbor.IsAvailable || _closedSet.Contains(neighbor))
                {
                    continue;
                }
                int _newMovementCostToNeighbour = _currentCell.GCost + getCellDistance(_currentCell, neighbor);
                if (_newMovementCostToNeighbour < neighbor.GCost ||
                    !_openSetList.Contains(neighbor))
                {
                    neighbor.GCost = _newMovementCostToNeighbour;
                    neighbor.HCost = getCellDistance(neighbor, _targetCell);
                    neighbor.ParentCell = _currentCell;
                    if (!_openSetList.Contains(neighbor))
                    {
                        _openSetList.Add(neighbor);
                    }
                }
            }
        }
    }
    private void retracePath(Cell startCell, Cell endCell)
    {
        _levelManager.ResetPath();
        _levelManager.Path.Clear();
        _currentRetracedCell = endCell;
        while (_currentRetracedCell != startCell)
        {
            _levelManager.Path.Add(_currentRetracedCell);
            _currentRetracedCell = _currentRetracedCell.ParentCell;
        }
        _levelManager.Path.Reverse();
        _levelManager.ShowPath();
    }
    private int getCellDistance(Cell cellA, Cell cellB)
    {
        _cellDistanceX = Mathf.Abs(cellA.IndexX - cellB.IndexX);
        _cellDistanceY = Mathf.Abs(cellA.IndexY - cellB.IndexY);

        if (_cellDistanceX > _cellDistanceY)
        {
            return 14 * _cellDistanceY + 10 * (_cellDistanceX - _cellDistanceY);
        }
        return 14 * _cellDistanceX + 10 * (_cellDistanceY - _cellDistanceX);
    }
}
