using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NiceSDK;
using Sirenix.OdinInspector;
public class ScrollUpdater : MonoBehaviour
{
    private HUDManager _hudManager;
    private void Awake()
    {
        _hudManager = HUDManager.Instance;
    }
    void Update()
    {
        if (transform.parent.GetChild(0) == transform || transform.parent.GetChild(transform.parent.childCount - 1) == transform)
        {
            if (Mathf.Abs(_hudManager.ScrollBorderUp.transform.position.y - transform.position.y) < 3 || Mathf.Abs(_hudManager.ScrollBorderDown.transform.position.y - transform.position.y) < 3)
            {
                _hudManager.CreateNewButtons();
                enabled = false;
            }
        }
    }
}