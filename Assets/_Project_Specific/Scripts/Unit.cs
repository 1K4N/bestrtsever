using UnityEngine;
using DG.Tweening;
using Sirenix.OdinInspector;
using NiceSDK;
using UnityEngine.Events;
#if UNITY_EDITOR
using UnityEditor;
#endif
public class Unit : MonoBehaviour
{
    [SerializeField] private UnitCollider _unitColliderPrefab;
    [SerializeField, ReadOnly] protected UnitCollider[] _unitColliderArr;
    [SerializeField] private Vector2 _buildingSize;
    protected LevelManager _levelManager;
    protected Cell _spawnCell;
    private GameConfig _gameConfig;
    private Tween _scaleTween;
    private Vector3 _buildPosition;

    protected eType _unitType;
    protected bool _isMoving;
    public bool IsMoving => _isMoving;
    public eType Type => _unitType;
    public enum eType
    {
        Barrack,
        PowerPlant,
        Shipyard
    }

#if UNITY_EDITOR
    [Button]
    protected virtual void setRef()
    {

    }
    [Button]
    private void setSize()
    {
        float tempOffset = GameConfig.Instance.Map.CellSize.x / 2;
        GetComponent<RectTransform>().sizeDelta = new Vector2(
            (_buildingSize.x - 1) * LevelManager.Instance.Map.GridOffSet + _buildingSize.x * GameConfig.Instance.Map.CellSize.x,
            (_buildingSize.y - 1) * LevelManager.Instance.Map.GridOffSet + _buildingSize.y * GameConfig.Instance.Map.CellSize.y);

        for (int i = 0; i < _unitColliderArr.Length; i++)
        {
            if (_unitColliderArr[i] != null)
            {
                DestroyImmediate(_unitColliderArr[i].gameObject);
            }
        }
        for (int i = 0; i < _buildingSize.x * _buildingSize.y; i++)
        {
            GameObject unitCollider = PrefabUtility.InstantiatePrefab(_unitColliderPrefab.gameObject, transform) as GameObject;
        }
        _unitColliderArr = GetComponentsInChildren<UnitCollider>(true);


        int _buildingColliderCount = 0;
        for (int i = 0; i < _buildingSize.y; i++)
        {
            for (int j = 0; j < _buildingSize.x; j++)
            {
                _unitColliderArr[_buildingColliderCount].GetComponent<RectTransform>().anchoredPosition = new Vector3(
                    j * (GameConfig.Instance.Map.CellSize.x + LevelManager.Instance.Map.GridOffSet) + tempOffset,
                    -i * (GameConfig.Instance.Map.CellSize.y + LevelManager.Instance.Map.GridOffSet) - tempOffset, 
                    0);
                _buildingColliderCount++;
            }
        }
    }
    private void OnValidate()
    {
        setRef();
    }
#endif

    #region UNITY INIT
    private void Awake()
    {
        _levelManager = LevelManager.Instance;
        _gameConfig = GameConfig.Instance;
        for (int i = 0; i < _unitColliderArr.Length; i++)
        {
            _unitColliderArr[i].SetUnitParent(this);
        }
    }
    protected virtual void OnEnable()
    {
        transform.localScale = new Vector3(.1f, .1f, 1f);
    }
    #endregion
    #region CLASS FUNCTIONS
    public virtual void Build(UnityAction action = null)
    {
        for (int i = 0; i < _unitColliderArr.Length; i++)
        {
            if (!_unitColliderArr[i].Ready)
            {
                returnUnit();
                return;
            }
        }
        for (int i = 0; i < _unitColliderArr.Length; i++)
        {
            _unitColliderArr[i].TargetCell.Occupie();
        }

        transform.SetParent(_levelManager.Map.BuildingParent);
        _levelManager.DeactivateSpawnIndicator();
        _buildPosition = (_unitColliderArr[0].TargetCell.transform.position + _unitColliderArr[_unitColliderArr.Length - 1].TargetCell.transform.position) / 2;

        DOTween.Sequence()
            .Append(transform.DOMove(_buildPosition, _gameConfig.Unit.BuildMoveTween.Duration)
                .SetEase(_gameConfig.Unit.BuildMoveTween.Ease))
            .OnComplete(()=>
            {
                if (action != null)
                {
                    action();
                }
            });
    }
    public void ActivateBuilding()
    {
        _scaleTween?.Kill();
        _scaleTween = transform.DOScale(Vector3.one, _gameConfig.Unit.UnitActivationScaleTweenData.Duration).SetEase(_gameConfig.Unit.UnitActivationScaleTweenData.Ease);
    }
    public virtual void SpawnUnit()
    {

    }
    protected void indicateSpawnCell()
    {
        for (int i = _unitColliderArr.Length - 1; i > -1; i--)
        {
            if (tagCell(_unitColliderArr[i].TargetCell.Index + _levelManager.Map.SizeY)) return;
            if (tagCell(_unitColliderArr[i].TargetCell.Index - _levelManager.Map.SizeY)) return;
            if (tagCell(_unitColliderArr[i].TargetCell.Index + 1)) return;
            if (tagCell(_unitColliderArr[i].TargetCell.Index - 1)) return;
        }
        _levelManager.DeactivateSpawnIndicator();
    }
    private bool tagCell(int cellIndex)
    {
        if (cellIndex < _levelManager.Map.CellCount && cellIndex > 0)
        {
            if (_levelManager.Map.GetCell(cellIndex).IsAvailable)
            {
                _levelManager.SetSpawnCell(_levelManager.Map.GetCell(cellIndex));
                return true;
            }
        }
        return false;
    }
    protected virtual void returnUnit()
    {

    }
    #endregion
}