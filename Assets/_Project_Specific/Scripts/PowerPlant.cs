using NiceSDK;

public class PowerPlant : PassiveUnit
{
    protected override void OnEnable()
    {
        base.OnEnable();
        _unitType = eType.PowerPlant;
    }
    protected override void returnUnit()
    {
        base.returnUnit();
        PoolManager.ReturnPowerPlant(this);
    }
}