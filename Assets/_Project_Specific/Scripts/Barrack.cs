using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using NiceSDK;
public class Barrack : SpawnerUnit
{
    protected override void OnEnable()
    {
        base.OnEnable();
        _unitType = eType.Barrack;
    }
    protected override void returnUnit()
    {
        base.returnUnit();
        PoolManager.ReturnBarrack(this);
    }
    protected override void setMovingUnit()
    {
        base.setMovingUnit();
        _spawnedMovingUnit = PoolManager.CreateSoldier(_levelManager.Map.SoldierParent, Vector3.zero);
    }
}