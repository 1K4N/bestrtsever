using System;
public class Heap<T> where T : IHeapItem<T>
{
    private T[] _items;
    private int _currentItemCount;
    private int _parentIndex;
    private T _parentItem;

    private int _itemAIndex;
    private T _firstItem;
    private int _childIndexLeft;
    private int _childIndexRight;
    private int _swapIndex;
    public int Count => _currentItemCount;
    public Heap(int i_MaxHeapSize)
    {
        _items = new T[i_MaxHeapSize];
    }

    #region CLASS FUNCTIONS
    public void Add(T item)
    {
        item.HeapIndex = _currentItemCount;
        _items[_currentItemCount] = item;
        sortUp(item);
        _currentItemCount++;
    }
    public T RemoveFirst()
    {
        _firstItem = _items[0];
        _currentItemCount--;
        _items[0] = _items[_currentItemCount];
        _items[0].HeapIndex = 0;
        sortDown(_items[0]);
        return _firstItem;
    }
    public void Clear()
    {
        for (int i = 0; i < _currentItemCount; i++)
        {
            _items[i] = default;
        }
        _currentItemCount = 0;
    }
    public bool Contains(T item)
    {
        return Equals(_items[item.HeapIndex], item);
    }
    public void UpdateItem(T item)
    {
        sortUp(item);
    }
    private void sortDown(T item)
    {
        while (true)
        {
            _childIndexLeft = item.HeapIndex * 2 + 1;
            _childIndexRight = item.HeapIndex * 2 + 2;
            _swapIndex = 0;

            if (_childIndexLeft < _currentItemCount)
            {
                _swapIndex = _childIndexLeft;

                if (_childIndexRight < _currentItemCount)
                {
                    if (_items[_childIndexLeft].CompareTo(_items[_childIndexRight]) < 0)
                    {
                        _swapIndex = _childIndexRight;
                    }
                }

                if (item.CompareTo(_items[_swapIndex]) < 0)
                {
                    swap(item, _items[_swapIndex]);
                }
                else
                {
                    return;
                }
            }
            else
            {
                return;
            }
        }
    }
    private void sortUp(T item)
    {
        _parentIndex = (item.HeapIndex - 1) / 2;
        while (true)
        {
            _parentItem = _items[_parentIndex];
            if (item.CompareTo(_parentItem) > 0)
            {
                swap(item, _parentItem);
            }
            else
            {
                break;
            }
            _parentIndex = (item.HeapIndex - 1) / 2;
        }
    }
    private void swap(T itemA, T itemB)
    {
        _items[itemA.HeapIndex] = itemB;
        _items[itemB.HeapIndex] = itemA;
        _itemAIndex = itemA.HeapIndex;
        itemA.HeapIndex = itemB.HeapIndex;
        itemB.HeapIndex = _itemAIndex;
    }
    #endregion
}

public interface IHeapItem<T> : IComparable<T>
{
    public int HeapIndex
    {
        get;
        set;
    }
}