using UnityEngine;
using NiceSDK;
using Sirenix.OdinInspector;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class Map : MonoBehaviour
{
    [SerializeField] private Cell _cellPrefab;
    [SerializeField] private int _row;
    [SerializeField] private int _column;
    [SerializeField] private int _offSet;
    [SerializeField] private float _navigationSpeed;
    [SerializeField, ReadOnly] private int _size;
    [SerializeField, ReadOnly] private Transform _buildingParent;
    [SerializeField, ReadOnly] private Transform _gridParent;
    [SerializeField, ReadOnly] private Transform _soldierParent;    
    [SerializeField, ReadOnly] private Cell[] _cellArray;

    private Vector3 _navigateDirection;
    private Vector3 _targetDirection;
    public int GridOffSet => _offSet;
    public int Size => _size;
    public int SizeX => _row;
    public int SizeY => _column;
    public Transform SoldierParent => _soldierParent;
    public Transform BuildingParent => _buildingParent;
    public Cell GetCell(int i_Index) => _cellArray[i_Index];
    public int CellCount => _cellArray.Length;
#if UNITY_EDITOR
    private void OnValidate()
    {
        setRef();
    }

    [Button]
    private void setRef()
    {
        _gridParent = transform.FindDeepChild<Transform>("Grids");
        _buildingParent = transform.FindDeepChild<Transform>("Buildings");
        _soldierParent = transform.FindDeepChild<Transform>("Soldiers");
        if (_row % 2 == 1)
        {
            _row -= 1;
        }
        if (_column % 2 == 1)
        {
            _column -= 1;
        }
        _size = _row * _column;
    }
    [Button]
    private void setMap()
    {
        float _sizeX = _cellPrefab.GetComponent<RectTransform>().sizeDelta.x;
        float _sizeY = _cellPrefab.GetComponent<RectTransform>().sizeDelta.y;
        int _index = 0;
        for (int i = 0; i < _cellArray.Length; i++)
        {
            if (_cellArray[i] != null)
            {
                DestroyImmediate(_cellArray[i].gameObject);
            }
        }
        for (int i = _row / 2; i > -_row / 2; i--)
        {
            for (int j = -_column / 2; j < _column / 2; j++)
            {
                GameObject _newCell = PrefabUtility.InstantiatePrefab(_cellPrefab.gameObject, _gridParent) as GameObject;
                _newCell.transform.localPosition = new Vector3((j * (_sizeX + _offSet)) + _sizeX / 2, (i * (_sizeY + _offSet)) - _sizeY / 2, 0);
                _newCell.transform.name = "Cell_" + _index;
                _index++;
            }
        }
        _cellArray = GetComponentsInChildren<Cell>(true);
        for (int i = 0; i < _cellArray.Length; i++)
        {
            _cellArray[i].SetIndex(i);
        }
    }
    [Button]
    private void clearMap()
    {
        for (int i = 0; i < _cellArray.Length; i++)
        {
            if (_cellArray[i] != null)
            {
                DestroyImmediate(_cellArray[i].gameObject);
            }
        }
        _cellArray = new Cell[0];
    }
#endif
    #region UNITY LOOP
    private void Update()
    {
        if (_navigateDirection != Vector3.zero)
        {
            _targetDirection = transform.localPosition + _navigateDirection * _navigationSpeed;
            transform.localPosition = Vector3.Lerp(transform.localPosition, _targetDirection, .6f);
        }
    }
    #endregion
    #region CLASS FUNCTIONS
    public void Navigate(Vector3 direction)
    {
        _navigateDirection = direction;
    }
    #endregion
}