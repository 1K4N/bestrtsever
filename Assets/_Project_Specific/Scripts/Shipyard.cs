using NiceSDK;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class Shipyard : SpawnerUnit
{
    protected override void OnEnable()
    {
        base.OnEnable();
        _unitType = eType.Shipyard;
    }
    protected override void returnUnit()
    {
        base.returnUnit();
        PoolManager.ReturnShipyard(this);
    }
    protected override void setMovingUnit()
    {
        base.setMovingUnit();
        _spawnedMovingUnit = PoolManager.CreateBoat(_levelManager.Map.SoldierParent, Vector3.zero);
    }
}