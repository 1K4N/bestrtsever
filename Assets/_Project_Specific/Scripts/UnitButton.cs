using UnityEngine;
using NiceSDK;
using UnityEngine.EventSystems;
using Sirenix.OdinInspector;

public class UnitButton : MonoBehaviour, IPointerUpHandler, IPointerDownHandler, IDragHandler
{
    [SerializeField, ReadOnly] private ScrollUpdater _scrollUpdater;
    [SerializeField] private eUnitType _unitType;
    private CameraManager _cameraManager;
    private HUDManager _hudManager;
    private LevelManager _levelManager;
    private GameConfig _gameConfig;
    private Unit _createdBuilding;
    private Vector2 _dragPosition;

    private bool _isCollided;
    public ScrollUpdater ScrollUpdater => _scrollUpdater;
    public bool IsCollided => _isCollided;
    public eUnitType UnitType => _unitType;
    public void SetCollided() => _isCollided = true;
    public enum eUnitType
    {
        Barrack,
        PowerPlant,
        Shipyard
    }
#if UNITY_EDITOR
    [Button]
    private void setRef()
    {
        _scrollUpdater = GetComponent<ScrollUpdater>();
    }
    private void OnValidate()
    {
        setRef();
    }
#endif
    #region UNITY INIT
    private void Awake()
    {
        _levelManager = LevelManager.Instance;
        _hudManager = HUDManager.Instance;
        _cameraManager = CameraManager.Instance;
        _gameConfig = GameConfig.Instance;
    }
    private void OnEnable()
    {
        _isCollided = false;
    }
    #endregion
    public void OnPointerDown(PointerEventData eventData)
    {
        switch (_unitType)
        {
            case eUnitType.Barrack:
                _createdBuilding = PoolManager.CreateBarrack(_levelManager.BuildingParent, Vector3.zero);
                break;
            case eUnitType.PowerPlant:
                _createdBuilding = PoolManager.CreatePowerPlant(_levelManager.BuildingParent, Vector3.zero);
                break;
            case eUnitType.Shipyard:
                _createdBuilding = PoolManager.CreateShipyard(_levelManager.BuildingParent, Vector3.zero);
                break;
            default:
                break;
        }
        RectTransformUtility.ScreenPointToLocalPointInRectangle(
            _hudManager.Canvas.transform as RectTransform, 
            Input.mousePosition,
            _cameraManager.MainCamera, 
            out _dragPosition);
        _createdBuilding.transform.localPosition = _dragPosition;
        _createdBuilding.ActivateBuilding();
        _hudManager.DeactivateOpenedMenu();
    }

    public void OnDrag(PointerEventData eventData)
    {
        RectTransformUtility.ScreenPointToLocalPointInRectangle(
            _hudManager.Canvas.transform as RectTransform, 
            Input.mousePosition,
            _cameraManager.MainCamera, 
            out _dragPosition);
        _createdBuilding.transform.localPosition = Vector3.Lerp(_createdBuilding.transform.localPosition, _dragPosition, _gameConfig.Unit.MovementDamping);
    }
    public void OnPointerUp(PointerEventData eventData)
    {
        _createdBuilding.Build();
    }
    #region CLASS FUNCTIONS
    public void ReturnUnitButton()
    {
        switch (_unitType)
        {
            case eUnitType.Barrack:
                PoolManager.ReturnBarrackButton(this);
                break;
            case eUnitType.PowerPlant:
                PoolManager.ReturnPowerPlantButton(this);
                break;
            case eUnitType.Shipyard:
                PoolManager.ReturnShipyardButton(this);
                break;
            default:
                break;
        }
    }
    #endregion
}
