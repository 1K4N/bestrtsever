using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using NiceSDK;
public class InfiniteScroll : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerExitHandler, IPointerEnterHandler, IScrollHandler
{
    private HUDManager _hudManager;
    private bool _isInputDown;
    private bool _isUpdatersActive;

    private void Awake()
    {
        _hudManager = HUDManager.Instance;
    }
    public void OnPointerDown(PointerEventData eventData)
    {
        _isInputDown = true;
        _isUpdatersActive = true;
        _hudManager.ActivateUnitButtonUpdaters();
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (!_isInputDown)
        {
            _hudManager.DeactivateUnitButtonUpdaters();
            _isUpdatersActive = false;
        }
    }
    public void OnPointerUp(PointerEventData eventData)
    {
        _isInputDown = false;
        _hudManager.DeactivateUnitButtonUpdaters();
        _isUpdatersActive = false;
    }
    public void OnPointerEnter(PointerEventData eventData)
    {
        _hudManager.ActivateUnitButtonUpdaters();
        _isUpdatersActive = true;
    }

    public void OnScroll(PointerEventData eventData)
    {
        if (!_isUpdatersActive)
        {
            _hudManager.ActivateUnitButtonUpdaters();
            _isUpdatersActive = true;
        }
    }
}
