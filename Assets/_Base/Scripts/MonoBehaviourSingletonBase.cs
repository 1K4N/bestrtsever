using UnityEngine;

namespace NiceSDK
{
    public abstract class MonoBehaviourSingletonBase<T> : MonoBehaviour where T : MonoBehaviour
    {
        protected virtual void Awake()
        {
        }
    }
}