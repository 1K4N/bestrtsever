using DG.Tweening;
using System;
using UnityEngine;

[Serializable]
public struct DefaultTween
{
    public float Duration;
    public Ease Ease;
}
[Serializable]
public struct DefaultTweenValue
{
    public float Value;
    public float Duration;
    public Ease Ease;
}
[Serializable]
public struct DefaultTweenDelay
{
    public DefaultTween Tween;
    public float Delay;
}
[Serializable]
public struct DefaultTweenPunch
{
    public DefaultTween Tween;
    public Vector3 Strength;
    public int Vibrato;
}